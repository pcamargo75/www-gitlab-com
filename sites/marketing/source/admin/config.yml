backend:
 name: gitlab
 repo: gitlab-com/www-gitlab-com # Path to your GitLab repository
 auth_type: implicit # Required for implicit grant
 app_id: # Application ID from your GitLab settings

# Allows access to /admin/ on local dev server
# Run npx netlify-cms-proxy-server from the root directory in separate terminal window
# https://www.netlifycms.org/docs/beta-features/#working-with-a-local-git-repository
local_backend: true

#media_folder: source/images/uploads
#public_folder: /images/uploads

publish_mode: editorial_workflow

media_folder: /source/images/uploads
public_folder: /images/uploads

collections:
  - name: topic
    label: Topic
    format: yml
    extension: yml
    folder: /data/topic/
    create: true
    slug: '{{file_name}}'
    media_folder: /source/images/topics
    public_folder: /images/topics
    fields:
      - {label: SEO Description, hint: This is the SEO description that will get rendered in search results, name: description, widget: string, required: true}
      - {label: Canonical Path, hint: Absolute path and it is always unique,  name: canonical_path, widget: string, required: true}
      - {label: Final URL Path, hint: This will the be final part of the topics page url - about.gitlab.com/topics/URL, name: file_name, widget: string, required: true}
      - {label: Social share image, hint: The images that is displayed in social media when this article is shared, name: twitter_image, widget: image, media_folder: /source/images/opengraph, public_folder: /images/opengraph, required: true}
      - {label: Title, name: title, hint: Title displayed in header of webpage, widget: string, required: true}
      - {label: Header Body, name: header_body, widget: markdown, required: true}
      - label: Related Content, links in header to outside sources
        name: related_content
        hint: Links displayed in the header underneath the "MORE ON THIS TOPIC" title
        widget: list
        required: false
        fields:
          - {label: Title, name: title, widget: string, required: false}
          - {label: URL, name: url, widget: string, required: false}
      - {label: Cover Image, hint: Image displayed at beginning of body content, name: cover_image, widget: image, required: false}
      - {label: Body, name: body, widget: markdown, required: true}
      - label: pull_quote
        name: pull_quote
        widget: list
        required: false
        hint: Pull quotes displayed after body content
        fields:
          - {label: quote, name: quote, widget: string, required: false}
          - {label: source, name: source, widget: string, required: false}
      - {label: Benefits Title, hint: Title displayed next to list of benefits for this topic,  name: benefits_title, widget: string, required: false}
      - {label: Benefits Description, name: benefits_description, widget: markdown, required: false}
      - label: benefits list
        name: benefits
        widget: list
        media_folder: /source/images/icons
        public_folder: /images/icons
        required: false
        hint: Each of these items will be displayed next to their image, underneath the body content
        fields:
          - {label: Title, name: title, widget: string, required: true}
          - {label: Description, name: description, widget: markdown, required: true}
          - {label: Image, name: image, widget: image, required: true}     
      - label: cta_banner
        name: cta_banner
        widget: list
        required: false
        hint: More content to be displayed after benefits of block, this content can have CTA buttons
        fields:
          - {label: Title, name: title, widget: string, required: false}
          - {label: CTA Body, name: body, widget: markdown, required: false}
          - label: CTA buttons
            name: cta
            widget: list
            required: false
            fields:
              - {label: CTA Text, name: text, widget: string, required: false}
              - {label: URL, name: url, widget: string, required: false}
      - {label: Resources title, hint: Title of resources block displayed at the bottom of the page, name: resources_title, widget: string, required: false}
      - {label: Resources description, name: resources_intro, widget: markdown, required: false}
      - label: Resources
        name: resources
        widget: list
        required: false
        hint: Each of these will be grouped by type and displayed in order as shown in the admin. You can drag and drop the items in the admin.
        fields:
          - {label: Title, name: title, widget: string, required: true}
          - {label: URL, name: url, widget: string, required: true}
          - {label: Type, name: type, widget: "select", options: ["Webcast", "Video", "Whitepapers", "Books", "Case studies", "Articles", "Reports", "Blog", "Podcast"], required: true}
      - label: Suggested Content
        name: suggested_content
        hint: Only place existing blog articles starting with the /blog/path-to-blog-article path. This block will intelligently pull the blog title, description, link, and image into a blog card displayed in a group at the bottom of the page.
        widget: list
        required: false
        fields:
          - {label: URL, name: url, widget: string}
      - label: schema.org FAQPage questions
        name: schema_faq
        hint: questions displayed in search results. DO NOT INCLUDE LINKS IN BODY. Use CTA button instead.
        widget: list
        fields:
          - {label: Question, name: question, widget: string}
          - {label: Answer, name: answer, widget: markdown, hint: DO NOT INCLUDE LINKS IN BODY, USE CTA BUTTON}
          - label: CTA buttons
            name: cta
            widget: list
            required: false
            fields:
              - {label: CTA Text, name: text, widget: string, required: false}
              - {label: URL, name: url, widget: string, required: false}
  - name: blog_posts
    label: Blog Posts
    preview_path: "blog/{{year}}/{{month}}/{{day}}/{{filename}}/"
    folder: sites/marketing/source/blog/blog-posts/
    extension: .html.md.erb
    format: frontmatter
    create: true
    slug: '{{year}}-{{month}}-{{day}}-{{title}}'
    view_filters:
      - label: "Company"
        field: categories
        pattern: 'company'
      - label: "Culture"
        field: categories
        pattern: 'culture'
      - label: "Engineering"
        field: categories
        pattern: 'engineering'
      - label: "Insights"
        field: categories
        pattern: 'insights'
      - label: "News"
        field: categories
        pattern: 'news'
      - label: "Open Source"
        field: categories
        pattern: 'open source'
      - label: "Security"
        field: categories
        pattern: 'security'
      - label: "Unfiltered"
        field: categories
        pattern: 'unfiltered'
    media_folder: /source/images/blogimages
    public_folder: /images/blogimages
    fields:
      - {label: Title, name: title, widget: string, required: true}
      - {label: Author, name: author, widget: string, required: false}
      - {label: Author GitLab, name: author_gitlab, widget: string, required: false}
      - {label: Author Twitter, name: author_twitter, widget: string, required: false}
      - {label: Categories, name: categories, widget: string, required: true, hint: Visit https://about.gitlab.com/handbook/marketing/blog/#categories for all categories.}
      - {label: Tags, name: tags, widget: string, required: false, hint: Visit https://about.gitlab.com/handbook/marketing/blog/#tags see all available tags. Seperate by commas for multiple tags}
      - {label: Description, name: description, widget: string, required: true}
      - {label: Image, name: image_title, widget: image, required: false}
      - {label: Twitter Text, name: twitter_text, widget: string, required: false}
      - {label: Body, name: body, widget: markdown, required: true}
  