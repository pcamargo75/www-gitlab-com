---
title: "GitLab's Functional Group Updates - March 13th - 23rd"
author: Kirsten Abma
author_gitlab: kirstenabma
categories: company
description: "The Functional Groups at GitLab give an update on what they've been working on from March 13th - 23rd"
---

<!-- beginning of the intro - leave it as is -->

Every day from Monday to Thursday, right before our [GitLab Team call](/handbook/#team-call), a different Functional Group gives an [update](/handbook/people-operations/group-conversations/) to our team.

The format of these calls is simple and short where they can either give a presentation or quickly walk the team through their agenda.

<!-- more -->

## Recordings

All of the updates are recorded using [Zoom](https://zoom.us) at the time of the call. All the recordings will be uploaded to our YouTube account and made public, with the exception of the Sales and Finance updates.

<!-- end of the intro -->

<!-- beginning of the FG block - repeat as many times as necessary (copy and paste the entire block) -->

----

### Backend / Platform Team

[Presentation slides](https://docs.google.com/presentation/d/1Spa5s37rDE0OmvNEZMd_rS_KSoNHvq-K1hSUZ5VL9OY/edit)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/LLMQEO7JFps" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<!-- end of the FG block -->

<!-- beginning of the FG block - repeat as many times as necessary (copy and paste the entire block) -->

----

### Infrastructure Team

[Presentation slides](https://docs.google.com/presentation/d/11uHv7W1ed4K9KVfaEMx7cJkXJVi1JBZk0RwAUwvdhI4/pub?start=false&loop=false&delayms=3000&slide=id.g153a2ed090_0_88)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/jweXnUqLE0U" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<!-- end of the FG block -->

----

### UX Team

[Presentation slides](https://docs.google.com/presentation/d/1LM6wHxGVRwHwQvGJiXwF_HEsbP8OEfW3p6WbdNSkozU/edit?usp=sharing)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/TMdw-plNfDQ" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<!-- end of the FG block -->

<!-- beginning of the FG block - repeat as many times as necessary (copy and paste the entire block) -->

----

### CI Team

[Presentation slides](https://docs.google.com/presentation/d/178K_5YeshDwrBvfNPkBgAfYIEIw1UdySWSbgGLsB2pU/edit)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/P1-Gl-rCnhc" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<!-- end of the FG block -->

<!-- beginning of the FG block - repeat as many times as necessary (copy and paste the entire block) -->

----

### Prometheus Team

[Presentation slides](https://docs.google.com/presentation/d/1ayrnWu2TCRlhJA4HKpNpISFBmBLmXVkhOefckyu6SSo/edit)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/K2GOEGTEz2w" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<!-- end of the FG block -->

<!-- beginning of the FG block - repeat as many times as necessary (copy and paste the entire block) -->

----

### Support Team

[Presentation slides](https://docs.google.com/presentation/d/1EizMPiTJFYm7R7Av6J7DguR_Crgo_t8pufLYKoGC5sU/edit?usp=sharing)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/vN3BzuDouNk" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<!-- end of the FG block -->

<!-- beginning of the FG block - repeat as many times as necessary (copy and paste the entire block) -->

----

Questions? Leave a comment below or tweet [@GitLab](https://twitter.com/gitlab)! Would you like to join us? Check out our [job openings](/jobs/)!
