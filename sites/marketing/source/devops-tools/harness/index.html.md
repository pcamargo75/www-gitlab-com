---
layout: markdown_page
title: "Harness"
---
<!-- This is the template for defining sections which will be included in a tool comparison page. This .md file is included in the top of the page and a table of feature comparisons is added directly below it. This template provides the sections which can be included, and the order to include them. If a section has no content yet then leave it out. Leave this note in tact so that others can see where new sections should be added.

## Summary
   - minimal requirement <-- comment. delete this line
## Strengths
## Weaknesses
## Who buys and why
## Comments/Anecdotes
   - possible customer issues with product  <-- comment. delete this line
   - sample benefits and success stories  <-- comment. delete this line
   - date, source, insight  <-- comment. delete this line
## Resources
   - links to communities, etc  <-- comment. delete this line
   - bulleted list  <-- comment. delete this line
## FAQs
 - about the product  <-- comment. delete this line
## Integrations
## Pricing
   - summary, links to tool website  <-- comment. delete this line
### Value/ROI
   - link to ROI calc?  <-- comment. delete this line
## Questions to ask
   - positioning questions, traps, etc.  <-- comment. delete this line
## Comparison
   - features comparison table will follow this <-- comment. delete this line

<!------------------Begin page additions below this line ------------------ -->

## On this page
{:.no_toc}

- TOC
{:toc}

## Summary

Harness.io is a Continuous Integration product that is available both as SaaS and on-premises deployment (Connected On-Premises & Disconnected On-Premises).  The basic value prop for Harness is that it abstracts away some of the complexity involved in deploying both traditional applications and in microservices based applications.  Harness' stregth is more in the newer microservices based architectures.
Harness provides a good user interface to perform most Continuous Integration tasks and also connects with various SCM and CI tools.

## Strengths

- Strong continuous integration product with features normally found in all CI products.
- Easy to use UI, Visual display of pipelines and pipeline progress.
- Ability to customize and templatize pipelines and pipeline steps
- Continuous Verification capability integrates with several monitoring tools and applies machine learning to detect anamolies and automate rollback of deployment.


## Gaps

- No SCM or CI capability - context switching from SourceCode to CI to CD tools to take a pipeline from code to production.  Product assumes the artifacts are built, just need to be deployed.
- Several out of the box integrations, but feedback from those systems is via a console type interface, making it harder to monitor tasks and to track down root cause of problems during deployment.
- No built in security - must rely on third party products.  This lengthens time to secure, test and deploy.
- Issue management with pipelines requires third party product.  This could result in loss of context and delays in addressing problems.

## Comparison
