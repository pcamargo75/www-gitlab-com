---
layout: markdown_page
title: "Why Ultimate?"
description: "Ultimate enables enterprises maintain a consistent DevOps experience with a single, scalable interface for end to end DevOps. Learn more!"
canonical_path: "/pricing/ultimate/"
---

GitLab Ultimate is ideal for projects with executive visibility and strategic organizational usage. Ultimate enables enterprises transform IT by optimizing and accelerating delivery while managing priorities, security, risk, and compliance.

In addition to the capabilities in lower tiers, GitLab Ultimate adds security capabilities like SAST, DAST, Dependency scanning, container scanning and a comprehensive Security Dashboard to provide a snapshot of the Security posture. It also helps organizations to manage their compliance posture through License compliance and a compliance dashboard. Organizations can improve efficiency through multi level epics to organize and prioritise initiatives as well as improve developer efficiency via productivity insights. Free guest user licenses help to improve your license usage for users with minimal interaction with the system.

Please note this is not a comprehensive set of capabilities in GitLab Ultimate, visit [about.gitlab.com/features](https://about.gitlab.com/features/) for the latest. GitLab continuously adds features every month and evaluates features that can be moved to lower tiers to benefit more users.

Ultimate enables enterprises achieve advanced DevOps maturity by introducing enterprise capabilities to:

## Increase Operational Efficiencies
Ultimate enables enterprises to maintain a consistent DevOps experience with a single, scalable interface for end to end DevOps. With this, developer satisfaction and productivity is massively improved with more time for value added projects rather than spending time learning different toolsets and integrating them. Additionally, DevOps Dashboards & Analytics available in Ultimate facilitate greater visibility and transparency across projects - helping to improve efficiencies and eliminate bottlenecks in the organization. Ultimate introduces Project Insights and Portfolio Management in the product to increase organizational efficiencies.
> **~20 tools consolidated into GitLab; Glympse remediated security issues faster than any other company in their Security Auditor's experience** <br><br> Development can move much faster when engineers can stay on one page and click buttons to release auditable changes to production and have easy rollbacks; everything is much more streamlined. Within one sprint, just 2 weeks, Glympse was able to implement security jobs across all of their repositories using GitLab’s CI templates and their pre-existing Docker-based deployment scripts.<br> ***Zaq Wiedmann*** <br> Lead Software Engineer, Glympse <br> [Read more](/customers/glympse/)

## Deliver Better Products Faster
Ultimate enables seamless collaboration across Dev and Ops with continuous innovation. This continuous feedback loop from customers, deployments and monitoring operations back into development helps enterprises create products relevant for customers, go to market faster and beat competition as well. With every change undergoing rigorous testing, security & compliance testing, and incremental deployment - Ultimate allows rapid, iterative deployment of better quality software. Ultimate enhances configuration and monitoring to deliver products faster.
>  **Jenkins build took 3 hours, now with GitLab it takes 30 mins: a 6x improvement** <br><br>GitLab has allowed Alteryx to have code reviews, source control, continuous integration, and continuous deployment all tied together and speaking the same language. The team took a build that was running legacy systems and moved it to GitLab. This build took 3 hours on the Jenkins machine and it took 30 minutes to run on GitLab after it was going. Engineers can actually look at the build and understand what’s going on; they’re able to debug it and make it successful <br> [Read more](/customers/alteryx/)

## Reduce Security & Compliance Risk
Ultimate introduces capabilities to help reduce and manage risk from security and regulatory compliance by identifying vulnerabilities and compliance issues before the code ever leaves the developer's hands. Ultimate introduces Application Security Testing, Security Dashboards, and more advanced common controls and audit to reduce risks.
>  **BI Worldwide performed 300 SAST/Dependency scans in the first 4 days of use helping the team identify previously unidentified vulnerabilities**<br><br> One tool for SCM+CI/CD was a big initial win. Now wrapping security scans into that tool as well has already increased our visibility into security vulnerabilities. The integrated Docker registry has also been very helpful for us. Issue/Product management features let everyone operate in the same space regardless of role. <br> ***Adam Dehnel*** <br> *Product architect, BI Worldwide* <br> [Read more](/customers/bi_worldwide/)

Ultimate also includes priority support (4 business hour support), live upgrade assistance, and a Technical Account Manager who will work with you to achieve your strategic objectives and gain maximum value from your investment in GitLab. _The Technical Account Manager is available to Ultimate customers spending $50,000 per year or more on their license._

Read all case studies [here](/customers/)

# Ultimate Specific Features

The below list of features are after factoring in the announcement regarding [18 GitLab features moving to core](https://about.gitlab.com/blog/2020/03/30/new-features-to-core/). The timelines of the actual move of features to core in the product will be as per the linked issues in the announcement.

#### Security

| Cybersecurity is a critical concern of every business leader.  Your applications MUST be secure. GitLab Ultimate weaves security into the pipeline to provide early and actionable feedback to the development team.  | [![Security Dashboards](/images/feature_page/screenshots/61-security-dashboard.png)](https://about.gitlab.com/images/feature_page/screenshots/61-security-dashboard.png) |

| Features    | Value |
| --------- | ------------ |
| [Static Application Security Testing](https://docs.gitlab.com/ee/user/application_security/sast/) | Evaluates the static code, checking for potential security issues.   |
| [Dynamic Application Security Testing](https://docs.gitlab.com/ee/user/application_security/dast/) | Analyzes the review application to identify potential security issues - both automated as part of the pipeline as well as on demand  |
| [Dependency Scanning](https://docs.gitlab.com/ee/user/application_security/dependency_scanning/) |  Evaluates the third-party dependencies to identify potential security issues.   |
| [Container Scanning](https://docs.gitlab.com/ee/user/application_security/container_scanning/) |  Analyzes Docker images and checks for potential security issues, offers a suggested solution for the vulnerability, when available.  |
| [Security Center](https://docs.gitlab.com/ee/user/application_security/security_dashboard/#instance-security-center) | Visualize the latest security status for each project and across projects including a Security Dashboard, vulnerability report and settings. |
| [API Fuzz testing](https://docs.gitlab.com/ee/user/application_security/api_fuzzing/index.html) | API Fuzz Testing is a great way to find bugs and vulnerabilities in your web apps and APIs that other scanners and testing techniques miss |
| [Coverage Guided Fuzz testing](https://docs.gitlab.com/ee/user/application_security/coverage_fuzzing/) | Coverage-guided fuzzing sends random inputs to an instrumented version of your application in an effort to cause unexpected behavior, such as a crash |

#### Compliance

| The compliance tools provided by GitLab let you keep an eye on various aspects of your project. | ![License Compliance](https://docs.gitlab.com/ee/user/compliance/license_compliance/img/license_compliance_pipeline_tab_v13_0.png) |

| Features    | Value |
| --------- | ------------ |
| License Compliance |  Identify the presence of new software licenses included in your project and track project dependencies. Approve or deny the inclusion of a specific license. |
| Compliance Dashboard |  Compliance dashboard gives you the ability to see your group’s Merge Request activity by providing a high-level view for all projects in the group and approvers for the merge request |
| [Credential Inventory(Self Managed Only)](https://docs.gitlab.com/ee/user/admin_area/credentials_inventory.html#credentials-inventory-ultimate-only) |  Credential Inventory allows you to keep track of all the credentials, revoke and delete credentials based on access scope and expiry |

#### Project, Portfolio and Requirements Management

| Establish end to end visibility from Business Idea to delivering value. GitLab Ultimate, enables portfolio planning, tracking, and execution in one tool, that unifies the team to focus on delivering business value. | [![Epics](/images/feature_page/screenshots/51-epics.png)](https://about.gitlab.com/images/feature_page/screenshots/51-epics.png) |

| Features    | Value |
| --------- | ------------ |
| [Multi Level Epics](https://docs.gitlab.com/ee/user/group/epics/#multi-level-child-epics) |  Organize, plan, and prioritize business ideas and initiatives into multi level epics. |
| [Requirements Management](https://docs.gitlab.com/ee/user/project/requirements/index.html) | Simple and intuitive way to create and trace your requirements throughout the entire Software DevOps lifecycle.|
| [Project Insights](https://docs.gitlab.com/ee/user/project/insights/) | Visualize project insights to improve developer efficiencies.   |
| [Test Cases](https://docs.gitlab.com/ee/ci/test_cases/index.html) | Create, modify and view testing scenarios within GitLab itself |



#### Other

| Features    | Value |
| --------- | ------------ |
| [Free Guest Users](https://docs.gitlab.com/ee/user/permissions.html#free-guest-users)|  Guest users don’t count towards the license count.  |
| [Status Page](https://docs.gitlab.com/ee/operations/incident_management/status_page.html)| Create and deploy a static website to communicate efficiently to users during an incident. |
| [Auto Rollback](https://docs.gitlab.com/ee/ci/environments/#auto-rollback)| Automatic rollback to the last successful deployment. |




<center><a href="/sales" class="btn cta-btn orange margin-top20">Contact sales and learn more about GitLab Ultimate</a></center>
