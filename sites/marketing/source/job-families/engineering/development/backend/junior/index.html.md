---
layout: job_family_page
title: Junior Backend Engineer
---

Backend Engineers at GitLab work on our product. This work includes both the open source version of GitLab, the enterprise editions, and the GitLab.com service as well. Backend Engineers work with their peers on teams dedicated to an area of the product. They work together with product managers, designers, frontend engineers, and other colleagues towards common goals.

Junior Backend Engineers share the same requirements and responsibilities as an [Intermediate Backend Engineer](/job-families/engineering/development/backend/intermediate/), but typically join with less or alternate experience.

## Job Grade
The Junior Backend Engineer is a [grade 5](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

## Responsibilities
* Develop features and improvements to the GitLab product in a secure, well-tested, and performant way
* Collaborate with Product Management and other stakeholders within Engineering (Frontend, UX, etc.) to maintain a high bar for quality in a fast-paced, iterative environment
* Advocate for improvements to product quality, security, and performance
* Solve technical problems of moderate scope and complexity.
* Craft code that meets our internal standards for style, maintainability, and best practices for a high-scale web environment. Maintain and advocate for these standards through code review.
* Recognize impediments to our efficiency as a team ("technical debt"), propose and implement solutions
* Represent GitLab and its values in public communication around specific projects and community contributions.
* Ship small features and improvements with minimal guidance and support from other team members. Collaborate with the team on larger projects.
* Participate in Tier 2 or Tier 3 weekday and weekend and occasional night on-call rotations to assist troubleshooting product operations, security operations, and urgent engineering issues.

## Requirements
* Ability to use GitLab
* Significant professional experience with Ruby on Rails or language required by the specialty
* Professional experience with any other technologies that may be required by the specialty
* Proficiency in the English language, both written and verbal, sufficient for success in a remote and largely asynchronous work environment
* Demonstrated capacity to clearly and concisely communicate about complex technical, architectural, and/or organizational problems and propose thorough iterative solutions
* Experience with performance and optimization problems and a demonstrated ability to both diagnose and prevent these problems
* Comfort working in a highly agile, intensely iterative software development process
* Demonstrated ability to onboard and integrate with an organization long-term
* Positive and solution-oriented mindset
* Effective communication skills: Regularly achieve consensus with peers, and clear status updates
* An inclination towards communication, inclusion, and visibility
* Experience owning a project from concept to production, including proposal, discussion, and execution.
* Self-motivated and self-managing, with strong organizational skills.
* Demonstrated ability to work closely with other parts of the organization
* Share our values, and work in accordance with those values
* Ability to thrive in a fully remote organization

## Nice-to-haves
* Experience in a peak performance organization, preferably a tech startup
* Experience with the GitLab product as a user or contributor
* Product company experience
* Experience working with a remote team
* Enterprise software company experience
* Developer platform/tool industry experience
* Experience working with a global or otherwise multicultural team
* Computer science education or equivalent experience
* Passionate about/experienced with open source and developer tools

## Levels

* Junior
* [Intermediate](/job-families/engineering/development/backend/intermediate/)
* [Senior](/job-families/engineering/development/backend/senior/)
* [Staff](/job-families/engineering/development/backend/staff/)
* [Backend Manager, Engineering](/job-families/engineering/backend-engineer/#backend-manager-engineering)

## Specialties
Read more about what a [specialty](/handbook/hiring/#definitions) is at GitLab here.

### Distribution
The Distribution team closely partners with our greater engineering organization to build, configure and automate GitLab deployments. GitLab's distribution team is tasked with creating a seamless installation and upgrade experience for users across a multitude of platforms.

Distribution engineering regularly interfaces with broader development teams in supporting newly created features. Notably, our infrastructure team is the distribution team's biggest internal customer, so there is significant team interdependency. The Distribution team is involved with diverse projects and tasks that include assisting community packaging efforts. This is reflected in the job role:
* Development and maintenance of Cloud Native GitLab deployment tooling and upgrade methods.
* Support Omnibus GitLab package installations across multiple Linux based Operating Systems.
* Experience with various Cloud provider deployment methods including AWS Cloudformation, GCP Deployment Manager, Azure Resource Manager.

### Package
Package engineers are focused on creating the binary repository management system that will extend our Continuous Integration (CI) functionality to allow access and management of artifacts manipulated by projects.

By extending the current CI artifacts system, the Package team will expose GitLab as a package repository allowing access to the most common package managers, e.g. Maven and APT and similar. Additionally, the Package team is improving the Container Registry and is responsible for items listed under [Package product category](/handbook/product/categories/#package).

### Protect

Focus on security protection features for GitLab (including container security, container scanning, policy management, network security, and host security). This role will report to and collaborate directly with the Protect Engineering Manager.

### Secure
Focus on security features for GitLab. This role will specifically focus on security; if you want to work with Ruby on Rails and not security, please apply to our Backend Engineer role instead. This role will report to and collaborate directly with the Secure Engineering Manager.

### Configuration
The configuration team works on GitLab's Application Control Panel, Infrastructure Configuration features, our ChatOps product, Feature flags, and our entire Auto DevOps feature set. It is part of our collection of Ops Backend teams.

### CI/CD
CI/CD Backend Engineers are primarily tasked with improving the Continuous Integration (CI) and Continuous Deployment (CD) functionality in GitLab. Engineers should be willing to learn Kubernetes and Container Technology. CI/CD Engineers should always have three goals in mind:
* Provide value to the user and communicate such with product managers,
* Introduce features that work at scale and in untrusting environments,
* Always focus on defining and shipping [the Minimal Viable Change](/handbook/product/product-principles/#the-minimal-viable-change-mvc).

We, as a team, cover end-to-end integration of CI/CD in GitLab, with components being written in Rails and Go. We work on a scale of processing a few million of CI/CD jobs on GitLab.com monthly. CI/CD engineering is interlaced with a number of teams across GitLab. We build new features by following our [direction](/direction/#ci--cd). Currently, we focus on providing a deep integration of Kubernetes with GitLab:
* by automating application testing and deployment through Auto DevOps,
* by managing GitLab Runners on top of Kubernetes,
* by working with other teams that provide facilities to monitor all running applications,
* in the future implement A-B testing, feature flags, etc.

Additionally, we also focus on improving the efficiency, performance, and scalability of all aspects of CI/CD:
* Improve performance of developer workflows, e.g. faster CI testing, by improving parallelization,
* Improve performance of implementation, ex.:by allowing us to run 10-100x more in one year,
* Identify and add features needed by us, ex.:to allow us to test more reliably and ship faster.

The CI/CD Engineering Manager also does weekly stand-up with a team and product managers to talk about plan for the work in the upcoming week and coordinates a deployment of CI/CD related services with infrastructure team.

### Geo
[GitLab Geo](https://docs.gitlab.com/ee/gitlab-geo/README.html) is an enterprise product feature, built to help speed up the development of distributed teams by providing one or more read-only mirrors of a primary GitLab instance. This mirror (a Geo secondary node) reduces the time to clone or fetch large repositories and projects, or can be part of a Disaster Recovery solution.

### Growth
Growth Engineers work with a cross-functional team to influence the growth of GitLab as a business. In helping us iterate and learn rapidly, these engineers enable us to more effectively meet the needs of potential users.

### Engineering Productivity
Engineering Productivity Engineers are full-stack engineers primarily tasked with improving the productivity of the GitLab developers (from both GitLab Inc and the rest of the community), and making the GitLab project maintainable in the long-term.

### Memory
The Memory team is responsible for optimizing GitLab application performance by managing the memory resources required.  The team is also responsible for changes affecting the responsiveness of the application.

### Ecosystem
The Ecosystem team is responsible for seamless integration between GitLab and 3rd party products as well as making GitLab products available on cloud service providers’ marketplaces such as AWS. The team plays a critical role in developing APIs and SDK and expanding GitLab market opportunities.

### Gitaly
Gitaly is a service that handles git and other filesystem operations for GitLab instances, and aims to improve reliability and performance while scaling to meet the needs of installations with thousands of concurrent users, including our site GitLab.com. This position reports to the Gitaly Lead.

### Meltano (BizOps Product)
[Meltano](https://gitlab.com/meltano/meltano) is an early stage project at GitLab focused on delivering an open source framework for analytics, business intelligence, and data science. It leverages version control, data science tools, CI, CD, Kubernetes, and review apps.

A Meltano Engineer will be tasked with executing on the vision of the Meltano project, to bring the product to market.

### Database
A database specialist is an engineer that focuses on database related changes and improvements. You will spend the majority of your time making application changes to improve database performance, availability, and reliability.

Unlike the [Database Engineer](/job-families/engineering/database-engineer/) position the database specialist title has a balance of application development and knowledge of PostgreSQL. As such Ruby knowledge is absolutely required and deep PostgreSQL knowledge is equally important.

### Gitter
[Gitter](https://gitter.im) specialists are full-stack JavaScript developers who are able to write JavaScript code that is shared between multiple environments. Gitter uses a JavaScript stack running Node.js on the server, and bundled with webpack on the client. The [iOS](https://gitlab.com/gitlab-org/gitter/gitter-ios-app), [Android](https://gitlab.com/gitlab-org/gitter/gitter-android-app), macOS (Cocoa) and [Linux/Windows (NW.js)](https://gitlab.com/gitlab-org/gitter/desktop/) clients reuse [much of the same codebase](https://gitlab.com/gitlab-org/gitter/webapp) but also require some knowledge of Objective-C, Swift and Java. Gitter uses MongoDB, Redis, and Elasticsearch for backend storage.

### Infrastructure
[Infrastructure](/handbook/engineering/infrastructure/) specialists work alongside DBREs and SREs and are experienced Ruby/GoLang developers who work in the product with a focus on reliability, observability, performance and scalability at the application level, as well as on resource optimization from an Infrastructure perspective and on operationally relevant features.

### Delivery
Delivery specialist is an engineer that focuses on improving the engineering release workflows, creates new tools, improves release process and works closely with the whole Engineering team to ensure that every GitLab release reaches the public in time.

### Scalability
The Scalability team is responsible for optimising GitLab.com performance through
improving reliability, availability and performance of GitLab individual services
and application as a whole.

### Search
Elasticsearch engineers are focused on delivering a first class global search experience throughout GitLab products.  They are experienced Ruby/GoLang developers who focus on implementing core Elasticsearch functions while advising other development teams on best practices (e.g. indexing).

## Performance Indicators
* [Backend Unit Test Coverage](https://about.gitlab.com/handbook/engineering/development/performance-indicators/#backend-unit-test-coverage)
* [Mean Time to Merge (MTTM)](https://about.gitlab.com/handbook/engineering/development/performance-indicators/#mean-time-to-merge-mttm)

## Hiring Process
Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find their job title on our [team page](/company/team/).
* Qualified candidates will be invited to schedule a 30 minute [screening call](/handbook/hiring/interviewing/#screening-call) with one of our Global Recruiters.
* As part of the application, candidates are asked to complete a short technical questionnaire, with a possibility of additional technical questions being asked if needed after the application is submitted.
* Next, candidates will be invited to schedule a 30 minute screening call with one of our Technical Recruiters
* Next, candidates will be invited to schedule a 90 minute technical interview with one of our Backend Engineers
* Next, candidates will be invited to schedule a 60 minute interview with one of our Backend Engineering Managers
* Next, candidates will be invited to schedule a 60 minute interview with our Director of Engineering
* Successful candidates will subsequently be made an offer. Additional details about our process can be found on our hiring page.

Additional details about our process can be found on our [hiring page](/handbook/hiring).
