---
layout: handbook-page-toc
title: "Bizzabo"
description: "Bizzabo is primarily used by the Corporate Marketing team to manage events that have a paid ticket element &/or our internal Contribute event."
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

### Uses
Bizzabo is primarily used by the Corporate Marketing team to manage events that have a paid ticket element &/or our internal Contribute event. 


### Integration Setup 

Bizzabo is integrated with Marketo through API & Launchpoint services. 